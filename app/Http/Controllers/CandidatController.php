<?php

namespace App\Http\Controllers;

use App\Models\Candidat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CandidatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nom' => 'required',
        ];

        $validate = Validator::make($request->all(), $rules);

        if($validate->fails()){
            return response()->json([
                'warning'=>$validate->errors()
            ]);
        }

        $datas= [
            'nom' => $request->nom,
        ];
        
        try {
            Candidat::create($datas);
            $candidats = $this->getLastCandidates();

            return redirect()->route('qcm_staff', compact('candidats'));
        } catch (\Throwable $th) {
            return response()->json([
                'error'=>$th->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd("show");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nom = $request->nom;
        $rules = [
            'nom' => 'required',
        ];

        $validate = Validator::make($request->all(), $rules);

        if($validate->fails()){
            return response()->json([
                'warning'=>$validate->errors()
            ]);
        }

        $datas= [
            'nom' => $nom,
        ];
        
        try {
            $candidate = Candidat::find($id);
            $candidate->nom = $nom;
            $candidate->save();
            $candidats = $this->getLastCandidates();

            return redirect()->route('qcm_staff', compact('candidats'));
        } catch (\Throwable $th) {
            return response()->json([
                'error'=>$th->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getLastCandidates()
    {
        try {
            return $candidats = Candidat::orderByDesc('id')->limit(3)->get();
        } catch (\Throwable $th) {
            return response()->json([
                'error'=>$th->getMessage()
            ]);
        }
    }

    public function showQcmElaboration()
    {
        $candidats = $this->getLastCandidates();

        return view('qcm.staff.candidat.create', compact('candidats'));
    }
}
