<?php

namespace App\Http\Controllers;

use App\Models\Sujet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SujetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('qcm.staff.sujet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sujet = htmlentities($request->le_sujet);
        $titre = $request->titre;

        $rules = [
            'titre' => 'required',
            'le_sujet' => 'required',
        ];

        $validate = Validator::make($request->all(), $rules);

        if($validate->fails()){
            return response()->json([
                'warning'=>$validate->errors()
            ]);
        }

        $datas= [
            'titre' => $titre,
            'le_sujet' => $sujet,
        ];
        
        try {
            Sujet::create($datas);
            $lastSujet = $this->getLastSujet();

            return redirect()->route('question.create', compact('lastSujet'));
        } catch (\Throwable $th) {
            return response()->json([
                'error'=>$th->getMessage()
            ]);
        }
    }

    public function getLastSujet()
    {
        try {
            return $sujet = Sujet::latest()->first();
        } catch (\Throwable $th) {
            return response()->json([
                'error'=>$th->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
