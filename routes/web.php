<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SujetController;
use App\Http\Controllers\ReponseController;
use App\Http\Controllers\CandidatController;
use App\Http\Controllers\QuestionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('qcm.index');
});

Route::get('/qcm_staff', [CandidatController::class, 'showQcmElaboration'])->name('qcm_staff');

Route::resource('candidat', CandidatController::class);
Route::resource('sujet', SujetController::class);
Route::resource('question', QuestionController::class);
Route::resource('reponse', ReponseController::class);
Route::get('/reponse/add/{id_question}', [ReponseController::class, 'storeReponse'])->name('question.reponse.store');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
