<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>QCM-App</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('bootstrap-4.4.1-dist/css/bootstrap.min.css') }}" />
    <script src="{{ asset('jquery/jquery.js') }}"></script>
</head>
<body>
    <div class="container">
        @yield('content')
    </div>
</body>
<script src="{{ asset('ajax/popper.min.js') }} "></script>
<script src="{{ asset('bootstrap-4.4.1-dist/js/bootstrap.min.js') }}"></script>
@yield('js')
</html>