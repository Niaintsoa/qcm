@extends('layouts.qcm_app')
@section('content')
    <div class="modal fade" id="reponseModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form id="updateCandidate" class="form" method="POST">
                    @csrf
                    @method('PUT')
                    {{-- HTML Forms does not support PUT, PATCH, DELETE -- so we define Form method Spoofing --}}

                    <div class="modal-header">
                        <h4 class="modal-title">Ajouter une réponse</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="col-mr-2">
                            <label for="nom">La réponse</label>
                            <input type="text" name="reponse" id="reponse" class="form-control" >
                        </div>
                        <div class="col-mr-2">
                            <label for="nom">Evaluer à:</label>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-warning" >Confirmer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <h2>Implémentation du question</h2>
    <hr>
    <h2>Le sujet:</h2>
    <p>Le titre: </p>
    {{ $lastSujet->titre ?? 'Aucun titre à afficher' }}
    <p>Le sujet: </p>
    @if ($lastSujet)
        {!! html_entity_decode($lastSujet->le_sujet) !!}
    @endif
    <span>Aucun sujet à afficher</span>
    <hr>
    <h2>Ajouter une question</h2>
    <form action="{{ route('question.store') }}" method="post">
        @csrf

        <div class="form-group">
            <label for="">Label du question</label>
            <input type="text" class="form-control" name="titre">
        </div>
        <div class="form-group">
            <label for="">La question</label>
            <textarea name="question" class="form-control" id="" cols="30" rows="5"></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Ajouter</button>
        </div>
    </form>
    <hr>
    <h3>Les questions à propos du sujet:</h3>
    <table class="table table-hover table-bordered">
        <thead>
            <th>Numéro</th>
            <th>Label Question</th>
            <th>La question</th>
            <th>Actions</th>
        </thead>
        @foreach ($questions as $item)
        <tbody>
            <td>{{ $item->id ?? '-' }}</td>
            <td>{{ $item->titre ?? '-' }}</td>
            <td>{{ $item->question ?? '-' }}</td>
            <td>
                <a id="addResponse" data-action="{{ route('reponse.store', $item->id) }}" class="btn btn-success btn-sm" data-toggle="modal" data-target="#reponseModal">Ajout des reponses</a>
            </td>
        </tbody>
        @endforeach
    </table>
@endsection
@section('js')
    <script>
        var $modalEdit = $('#updateCandidate')
        var $table = $("table").find("tr")
        var $edit = $table.find("#idToEdit")
        $edit.each(function(){
            $(this).click(function(){
                var action = $(this).data('action')
                var name = $(this).data('name')
                $modalEdit.attr('action', action)
                $modalEdit.find("#nomToUpdate").val(name)
            })
        })
    </script>
@endsection