@extends('layouts.qcm_app')
@section('content')
    <div class="modal fade" id="editModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form id="updateCandidate" class="form" method="POST">
                    @csrf
                    @method('PUT')
                    {{-- HTML Forms does not support PUT, PATCH, DELETE -- so we define Form method Spoofing --}}

                    <div class="modal-header">
                        <h4 class="modal-title">Modifier le candidat</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="col-mr-2">
                            <label for="nom">Nom du candidat</label>
                            <input type="text" name="nom" id="nomToUpdate" class="form-control" >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-warning" >Confirmer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <h2>Elaborer le qcm</h2>
    <hr>
    <h3>Ajout d'un candidat:</h3>
    <form id="addCandidate" class="form-inline" action="{{ route('candidat.store') }}" method="POST">
        @csrf
        <div class="col-mr-2">
            <label for="nom">Nom du candidat</label>
            <input type="text" name="nom" id="mom" class="form-control" placeholder="Ecrivez le nom ici ...">
        </div>
        <div class="col mt-4">
            <button class="btn btn-primary" type="submit" >
                Ajouter
            </button>
        </div>
    </form>
    <hr>
    <strong>Les 3 nouveaux candidats</strong> <br>
    <a href="">Voir la liste complète >></a>
    <table class="table table-hover table-bordered">
        <thead class="thead-dark">
            <th>Numéros</th>
            <th>Nom</th>
            <th>Actions</th>
        </thead>
        @if ($candidats)
            @foreach ($candidats as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->nom }}</td>
                <td id="action">
                    <a id="idToEdit" data-name="{{ $item->nom }}" data-action="{{ route('candidat.update', $item->id) }}" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editModal">Modifier</a>
                    <a href="" class="btn btn-danger btn-sm">Supprimer</a>
                </td>
            </tr>
            @endforeach
        @endif
    </table>
    <hr>
    <div class="btn_right_aligned">
        <a href="{{ route('sujet.create') }}" type="button" class="btn btn-primary btn-lg">Suivant</a>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            var $modalEdit = $('#updateCandidate')
            var $table = $("table").find("tr")
            var $edit = $table.find("#idToEdit")
            $edit.each(function(){
                $(this).click(function(){
                    var action = $(this).data('action')
                    var name = $(this).data('name')
                    $modalEdit.attr('action', action)
                    $modalEdit.find("#nomToUpdate").val(name)
                })
            })
        });
    </script>
@endsection