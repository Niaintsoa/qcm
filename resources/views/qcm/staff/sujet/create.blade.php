@extends('layouts.qcm_app')
@section('content')
    <h2>Elaborer le qcm</h2>
    <hr>
    <h3>Ajouter un sujet:</h3>
    <form id="addSujet" method="POST">
        @csrf

        <div class="form-group">
            <label for="">Titre du sujet</label>
            <input type="text" name="titre" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Le contenu</label>
            <div id="editor"></div>
            <input hidden type="text" name="le_sujet" id="sujet">
        </div>
        <button type="submit" id="addSujetBtn" class="btn btn-primary">Suivant</button>
    </form>
    <hr>
@endsection
@section('js')
<script src="{{ asset('ckeditor5-build-classic/ckeditor.js') }}"></script>
<script>
    ClassicEditor
        .create(document.querySelector('#editor'))
        .catch(error => {
            console.error(error)
        })

    var $btn = $("#addSujetBtn")
    
    // TODO: Handle button disabled when input not filled
    
    $btn.click(function(){
        var $ck = $(".ck-content")
        var $htmlVal = $ck.html()
        var sujet = $("#sujet")
        var $initialParagraph = $ck.find("p")
        var $initialBr = $initialParagraph.find("br")
        var ckFiller = $initialBr.data("cke-filler")
        if(ckFiller == true){
            sujet.val("")    
        }else{
            sujet.val($htmlVal)
        }
        $("#addSujet").attr("action", "{{ route("sujet.store") }}")
    })
</script>
@endsection